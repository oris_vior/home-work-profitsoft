package lesson1.hashTags;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TopFiveHashTagTest {

    TopFiveHashTag topFiveHashTag = new TopFiveHashTagImpl();

    @Test
    void findTopFiveHashTagsSuccessful() {
        List<String> actual = Arrays.asList(
                "#one", "#one","#one","#one","#one","#one","#one",
                "#two","#two","#two","#two","#two","#two","#two","#two","#two",
                "#three","#three","#three","#three","#three","#three","#three","#three","#three",
                "#four","#four","#four","#four","#four","#four","#four",
                "#five","#five","#five","#five","#five","#five","#five","#five","#five","#five","#five","#five",
                "notHashTags","notHashTags","notHashTags","notHashTags",
                "igor","igor","igor"
                );

        Map<String, Integer> expected = Map.ofEntries(
                Map.entry("#one" , 7),
                Map.entry("#two" , 9),
                Map.entry("#three", 9),
                Map.entry("#five", 12),
                Map.entry("#four", 7)
        );

        assertEquals(topFiveHashTag.findTopFiveHashTags(actual) , expected);
    }


    @Test
    void TopFiveHashTagFail() {
        assertThrows(IllegalArgumentException.class, () -> topFiveHashTag.findTopFiveHashTags(new ArrayList<>(Collections.singletonList(null))));
        assertThrows(IllegalArgumentException.class, () -> topFiveHashTag.findTopFiveHashTags(null));
    }
}