package lesson1.sortFigure;

import lesson1.sortFigure.geometricFigures.Cube;
import lesson1.sortFigure.geometricFigures.Cylinder;
import lesson1.sortFigure.geometricFigures.Shape;
import lesson1.sortFigure.geometricFigures.Sphere;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SortFigureByVolumeTest {

    SortFigureByVolume sortFigureByVolume = new SortFigureByVolumeImpl();

    @Test
    void sortFigureSuccessful() {
        Shape cylinder = new Cylinder(12, 12);
        Shape sphere = new Sphere(12);
        Shape cube = new Cube(12);


        List<Shape> actual = new ArrayList<>();

        actual.add(cylinder);
        actual.add(sphere);
        actual.add(cube);

        assertEquals(Arrays.asList(cube,cylinder,sphere), sortFigureByVolume.sortFigure(actual));
    }


    @Test
    void setSortFigureFail() {
        assertThrows(IllegalArgumentException.class, () -> sortFigureByVolume.sortFigure(null));
    }

}