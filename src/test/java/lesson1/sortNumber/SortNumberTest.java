package lesson1.sortNumber;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SortNumberTest {
    SortNumber sortNumber = new SortNumberImpl();

    @Test
    void sortIntegerSuccessful() {
        assertArrayEquals(new int[]{0, 1, 2, 4, 12, 85}, sortNumber.sortInteger(new int[]{1, 2, 85, -5, 0, 4, -5, 12, -9}));
    }

    @Test
    void sortIntegerFail() {
        assertThrows(IllegalArgumentException.class, () -> sortNumber.sortInteger(null));
    }
}