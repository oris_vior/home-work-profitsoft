package lesson1.hashTags;

import java.util.List;
import java.util.Map;

public interface TopFiveHashTag {

    /**
     * Написати метод, який на вхід приймає список рядків-текстів,
     * в яких можуть бути хеш-теги (слова, що починаються на знак "#").
     * Як результат, метод повинен повертати top-5 найчастіше згаданих хеш-тегів
     * із зазначенням кількості згадки кожного з них. Декілька однакових хеш-тегів
     * в одному рядку повинні вважатися однією згадкою. Написати unit-тести для цього методу.
     * @param list
     * @return  top-5 найчастіше згаданих хеш-тегів
     */
    Map<String, Integer> findTopFiveHashTags(List<String> list);
}
