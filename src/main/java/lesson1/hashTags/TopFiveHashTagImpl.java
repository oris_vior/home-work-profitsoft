package lesson1.hashTags;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TopFiveHashTagImpl implements TopFiveHashTag{
    @Override
    public Map<String, Integer> findTopFiveHashTags(List<String> list) {
        if (Optional.ofNullable(list).isPresent() && !list.contains(null)) {
            Map<String, Long> hashTagMap;
            LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();
            List<Long> numbers = new ArrayList<>();

            hashTagMap = list
                    .stream()
                    .collect(Collectors.filtering(e -> e.startsWith("#"), Collectors.groupingBy(Function.identity(), Collectors.counting())));

            for (Map.Entry<String, Long> entry : hashTagMap.entrySet()) {
                numbers.add(entry.getValue());
            }

            numbers.sort(Collections.reverseOrder());

            for (Long number : numbers) {
                for (Map.Entry<String, Long> entry : hashTagMap.entrySet()) {
                    if (entry.getValue().equals(number)) {
                        sortedMap.put(entry.getKey(), number.intValue());
                    }
                }
            }
            return sortedMap
                    .entrySet()
                    .stream()
                    .limit(5)
                    .collect(Collectors
                            .toMap(Map.Entry::getKey, Map.Entry::getValue));
        }
        else {
            throw new IllegalArgumentException();
        }

    }
}
