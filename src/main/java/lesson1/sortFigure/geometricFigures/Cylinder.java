package lesson1.sortFigure.geometricFigures;

import java.util.Map;

public class Cylinder implements Shape{
    private final double radius;
    private final double height;

    public Cylinder(double radius, double height) {
        this.radius = radius;
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double volume() {
        return Math.PI*Math.pow(radius, 2) * height;
    }
}
