package lesson1.sortFigure.geometricFigures;

public interface Shape {
    double volume();
}
