package lesson1.sortFigure.geometricFigures;

public class Cube implements Shape{

    private final double sideLength;

    public Cube(double sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public double volume() {
        return Math.pow(sideLength, 3);
    }

    public double getSideLength() {
        return sideLength;
    }
}
