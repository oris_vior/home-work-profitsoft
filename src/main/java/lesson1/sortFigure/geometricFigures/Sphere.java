package lesson1.sortFigure.geometricFigures;

public class Sphere implements Shape{

    private final double radius;

    public double getRadius() {
        return radius;
    }

    public Sphere(double radius) {
        this.radius = radius;
    }

    @Override
    public double volume() {
        return (4*Math.PI * Math.pow(radius, 3))/3;
    }
}
