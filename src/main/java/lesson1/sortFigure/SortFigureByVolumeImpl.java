package lesson1.sortFigure;

import lesson1.sortFigure.geometricFigures.Shape;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SortFigureByVolumeImpl implements SortFigureByVolume{

    @Override
    public List<Shape> sortFigure(List<Shape> list) {
        if(Optional.ofNullable(list).isPresent()) {
            return list.stream().sorted(Comparator.comparing(Shape::volume)).collect(Collectors.toList());
        }
        throw new IllegalArgumentException();
    }
}
