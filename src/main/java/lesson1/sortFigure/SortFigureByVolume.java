package lesson1.sortFigure;

import lesson1.sortFigure.geometricFigures.Shape;

import java.util.List;

public interface SortFigureByVolume {
    /**
     *  Реалізувати метод, який сортує геометричні 3d фігури за об'ємом.
     *  Метод приймає параметром колекцію довільних геометричних
     *  фігур (куб, кулю, циліндр). Написати unit-тести для методу.
     *
     * @param list
     * @return сортовані геометричні фігури
     */
    public List<Shape> sortFigure(List<Shape> list);
}
