package lesson1.sortNumber;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class SortNumberImpl implements SortNumber{

    @Override
    public int[] sortInteger(int[] array) {
        if (Optional.ofNullable(array).isPresent() && Arrays.stream(array).allMatch(Objects::nonNull)) {
            return Arrays.stream(array).filter(value -> value >= 0).sorted().toArray();
        }
        throw new IllegalArgumentException();
    }
}
