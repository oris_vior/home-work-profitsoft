package lesson1.sortNumber;

public interface SortNumber {

    /**
     *  Зробити метод,який приймає на вхід масив цілих чисел,
     * і повертає тільки ті з них, які позитивні (>=0),
     * відсортувавши їх за спаданням. Зробити unit-тести для цього методу.
     * @param array
     * @return sort number when >=0;
     */
    public int[] sortInteger(int[] array);
}
